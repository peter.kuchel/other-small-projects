import java.util.ArrayList;
import java.util.Arrays;

public class Display {

    String currentDisplay;

    public Display(){
    }

    public static void mainBanner(){
        System.out.println("==================");
        System.out.println("=== BLACK JACK ===");
        System.out.println("==================\n\n");
    }
    public static void loseMessage(){
        System.out.println("==============================");
        System.out.println("=== YOU HAVE NO MORE MONEY ===");
        System.out.println("==============================");
    }
    
    private String makePrintable(ArrayList<String> toConvert){
        return Arrays.toString(toConvert.toArray());
    }

    public void displayHands(String holder, ArrayList<String> currentHand){
        String hand = "";
        for (String card: currentHand){
            String h = "["+card+"]"+ " ";
            hand += h;
        }
        currentDisplay = makePrintable(currentHand);
        System.out.println(holder+" current hand is: \n"+hand+"\n");
        
    }
    public void dealerHit(){
        System.out.println("Dealer hits...");
    }
    public void draw(){
        System.out.println("--- Draw ---");
    }
    public void winMessage(String holder){
        System.out.println("--- "+holder+" wins! ---");
    }
    
    public void bustMessage(String holder){
        System.out.println("--- "+holder+" busts! ---");
    }
    public void showCurrentMoney(int money){
        System.out.println("Current money is: "+money);
    }
    
    public static void clearScreen(){
        if (!Debug.debugOn){
            try{
                Thread.sleep(3000);
            } catch (InterruptedException e){

            }
            try{
                final String os = System.getProperty("os.name");

                if (os.contains("Windows")){
                        try{
                            new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor();
                        }
                        catch (Exception e){
                            System.out.println(e);
                        }
                    } else {
                        Runtime.getRuntime().exec("clear");
                    }
            }
            catch (final Exception e){
                System.out.println(e);
            }
        }
    }
}
