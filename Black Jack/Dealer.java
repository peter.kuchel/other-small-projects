
public class Dealer extends CardHolder{

    public Dealer(){

    }

    public void showDealerHand(){
        showCurrentHand("Dealers");
    }

    public void dealerBust(){
        display.bustMessage("Dealer");
    }

    public void matchOrBeatPlayer(int currentPlayerScore){
        Boolean beatPlayer = false;
        calculateCardValues();
        do {

            if (cardScore > GameState.blackjack){
                setBustStatus(true);
                beatPlayer = true;

            } else if (cardScore == currentPlayerScore){
                beatPlayer = true; 
                setDrawStatus(true);
            
            } else if (cardScore > currentPlayerScore){
                beatPlayer = true; 

            } else {
                display.dealerHit();
                getDealtCard();
                calculateCardValues();
                showDealerHand();
                try{
                    Thread.sleep(1500);
                } catch (InterruptedException e){

                }
            }

        } while (!beatPlayer);
    }
    
}
