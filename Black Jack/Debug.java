import java.util.Arrays;

public class Debug {
    
    public static boolean debugOn = false; 
    public static boolean aceDebug = false; 

    public static void printCardVal(String card){
        if (debugOn){
            System.out.println(card);
        }
    }

    public static void printCurrentDeck(String [] deck){
        if (debugOn){
            String a = Arrays.toString(deck);
            System.out.println(a);
        }
    }
    public static void getHandValues(int count){
        if (debugOn){
            System.out.println("Current hand value is: "+count);
        }
    }
    public static void getNumOfAces(int count){
        if (debugOn){
            System.out.println("Current number of aces in hand is: "+count);
        }
    }
    public static void acesInHand(){}
    
}
