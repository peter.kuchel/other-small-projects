import java.util.Scanner;
import java.util.InputMismatchException;

public class Player extends CardHolder{
    Scanner in;
    public static int currentMoney = 50; 
    public static int currentBet; 

    public Player(){
        in = new Scanner(System.in);
    }

    public void showPlayerHand(){
        showCurrentHand("Player's");
    }

    public int getCardScore(){
        return cardScore; 
    }

    public void draw(){
        display.draw();
    }
    
    public void takeWinnings(){
        // add to total money 
        display.winMessage("Player");
        currentMoney += currentBet;
    }

    public void playerBust(){
        display.bustMessage("Player");

        // take away from bet
        currentMoney -= currentBet; 
    }

    public void makeBet(){
        display.showCurrentMoney(currentMoney);
        boolean currentBetGood = false;
        // int proposedBet;
        do {
            System.out.print("Place a bet: ");
            try{
                currentBet = in.nextInt();
                if (currentBet <= currentMoney && currentBet >=1){
                    currentBetGood = true;
                } else {
                    System.out.println("Place an acceptable bet");
                }
            }
            catch (InputMismatchException e){
                System.out.println("Place an acceptable bet");
                in.nextLine(); // so that it doesn't loop forever 
            }
            
        } while (!currentBetGood);
    }

    public void makeMove(){
        // reads the next line which is " "
        // because the cursor to get info is still there 
        in.nextLine();
        boolean choicesGood = false;
        
        while (!choicesGood){
            System.out.print("Hit (H) or Wave(W) :");
            String choice = in.nextLine();
            // System.out.println("Choice is: "+choice);

            switch(choice){
                case "H":
                    getDealtCard();
                    showPlayerHand();
                    calculateCardValues();
                    if (cardScore <= GameState.blackjack){
                        ;
                    } else {
                        setBustStatus(true);
                        choicesGood = true;
                    }
                    break; 

                case "W":
                    choicesGood = true;
                    calculateCardValues();
                    break; 

                default:
                    System.out.println("Input a valid choice");
            }
        }

        
    }
}

