import java.util.ArrayList;

public class CardHolder {

    protected int cardScore; 
    private boolean bust; 
    private boolean draw; 
    private int numberOfCards;
    private int [] aceValues; 
    protected Display display; 

    protected ArrayList<String> currentHand; 


    // constructor
    public CardHolder(){
        display = new Display();
        currentHand = new ArrayList<String>();
        setBustStatus(false);
        setDrawStatus(false);
    }

    protected void resetHand(){
        currentHand = new ArrayList<String>();
        setBustStatus(false);
        setDrawStatus(false);
    }

    protected void showCurrentHand(String holder){
        display.displayHands(holder, currentHand);
    }

    public void addCardToHand(String card){
        // Debug.printCardVal(card);
        currentHand.add(card);
    }

    protected void getDealtCard(){
        GameState.cardsDealt++;
        int nextCard = GameState.cardsDealt;
        String dealtCard = GameState.currentDeck[nextCard];
        currentHand.add(dealtCard);
    }

    protected void setBustStatus(boolean newStatus){
        bust = newStatus; 
    }
    protected void setDrawStatus(boolean newStatus){
        draw = newStatus;
    }
    public boolean getBustStatus(){
        return bust;
    }
    public boolean getDrawStatus(){
        return draw; 
    }

    private int getNumberOfAces(){
        int aceCount = 0;
        for (int i=0; i < numberOfCards; i++){
            String card = currentHand.get(i);
            if (card == Deck.ace){
                aceCount++;
            }
        }
        return aceCount;
    }

    private void caculateWithAces(int aces){
        // int i;
        
        aceValues = new int [aces];

        // get the current value of the aces into an array 
        for (int k=0; k < aces; k++){
            String aceValue = Deck.deckItems.get(Deck.ace);
            int value = Integer.parseInt(aceValue);
            
            aceValues[k] = value;
            // System.out.println("Ace val is: "+aceValues[k]);
        }

        boolean finalCalcMade = false;
        int trackAces = aces -1;
        int tempCardValue;
        do {
            tempCardValue = 0;

            for (int i=0; i < aceValues.length; i++){
                tempCardValue += aceValues[i];
            }
            // Debug.getHandValues(tempCardValue); 
            tempCardValue += cardScore;
            // Debug.getHandValues(tempCardValue);
            if (tempCardValue <= GameState.blackjack){
                finalCalcMade = true; 

            } else if (trackAces >= 0){
                aceValues[trackAces] = 1; //sets the value from 11 to 1 
                trackAces--;

            } else {
                finalCalcMade = true;
            }

        } while (!finalCalcMade);

        cardScore = tempCardValue;
        

    }
    protected void calculateCardValues(){
        cardScore = 0; 
        numberOfCards = currentHand.size();
        for (int i=0; i < numberOfCards; i++){
            String card = currentHand.get(i);
            // Debug.printCardVal(card);
            if (card != Deck.ace){
                String value = Deck.deckItems.get(card); 
                cardScore += Integer.parseInt(value);
            }
        }
        
        int aces = getNumberOfAces();
        Debug.getNumOfAces(aces);
        if (aces > 0){
            caculateWithAces(aces);
        } 
        Debug.getHandValues(cardScore);
    }

}
