import java.util.HashMap;
import java.util.Set;
import java.util.Random;
// import java.util.Collections;
// import java.util.Arrays; 

public class Deck {

    public static HashMap <String, String> deckItems;
    public static String ace = "A";
    private String [] deck;
    private final int numberOfSuites = 4; 
    private final int deckSize = 52;
    private final int maxTimesToShuffle = deckSize;
    private Random rand = new Random();

    public Deck(){
        setupDeckValues();
        deck = new String [deckSize];

        Set<String> keys = deckItems.keySet();
        int newSpot = 0;
        for (int i=0; i < numberOfSuites; i++){
            for (String k : keys) {
                deck[newSpot] = k;
                newSpot++;
            }
        }
        // Debug.printCurrentDeck(deck);

        //for testing the aces out if player or dealer gets multiple aces 
        if (Debug.debugOn && Debug.aceDebug){
            debugDeck();
        }
    }

    private void debugDeck(){
        int toAdd = 20;
        for (int j=0; j < toAdd; j++){
            int changeToAce = rand.nextInt(deckSize);
            deck[changeToAce] = ace;
        } 
    }

    public String [] getNewDeck(){
        shuffleDeck();
        // Debug.printCurrentDeck(deck);
        return deck; 
    }

    private void shuffleDeck(){

        for (int j=0; j < maxTimesToShuffle; j++){
            
            for (int i=0; i < deckSize; i++){
                int newRandom = rand.nextInt(deckSize);
                String tempPos = deck[i];
                deck[i] = deck[newRandom];
                deck[newRandom] = tempPos;
            }
        } 
    }

    private void setupDeckValues(){
        deckItems = new HashMap<String, String>();
        deckItems.put("2", "2");
        deckItems.put("3", "3");
        deckItems.put("4", "4");
        deckItems.put("5", "5");
        deckItems.put("6", "6");
        deckItems.put("7", "7");
        deckItems.put("8", "8");
        deckItems.put("9", "9");
        deckItems.put("10", "10");
        deckItems.put("J", "10");
        deckItems.put("Q", "10");
        deckItems.put("K", "10");
        deckItems.put("A", "11");
    }
}