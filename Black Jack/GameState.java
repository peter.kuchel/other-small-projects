
    
public class GameState {

    public static final int blackjack = 21;
    private boolean playerHasMoney = true; 

    private final int startCards = 4;
    public static int cardsDealt = 0; 
    
    public static String [] currentDeck;
    Deck gameDeck; 
    Player player; 
    Dealer dealer; 


    public GameState(){
        player = new Player();
        dealer = new Dealer();
        gameDeck = new Deck();
        // mainDisplay = new Display();
    }

    private void prepareNextRound(){
        cardsDealt = 0; 
        player.resetHand();
        dealer.resetHand();
    }

    private void dealStartHand(){
        for (int i=0; i < startCards; i++){
            String card = currentDeck[i];

            if (i%2==0){
                player.addCardToHand(card);
                
            } else {
                dealer.addCardToHand(card);
            }
            cardsDealt ++;
        }
    }
    private void checkPlayerMoney(){
        if (Player.currentMoney <= 0){
            playerHasMoney = false; 
        }
    }

    public void play(){
        Display.clearScreen();
        
        while (playerHasMoney){
            Display.mainBanner();
            currentDeck = gameDeck.getNewDeck();
            // Debug.printCurrentDeck(currentDeck);
            player.makeBet();
            dealStartHand();
            player.showPlayerHand();
            dealer.showDealerHand();
            player.makeMove();

            if (!player.getBustStatus()){

                int currentScore = player.getCardScore();
                dealer.matchOrBeatPlayer(currentScore);

                if (dealer.getDrawStatus()){
                    player.draw();

                } else if (dealer.getBustStatus()){

                    player.takeWinnings();

                } else {

                    player.playerBust();
                }

            } else {

                player.playerBust();
            }
            
            Display.clearScreen();
            prepareNextRound(); 
            checkPlayerMoney();
            
        

        }
        Display.loseMessage();

    }
}
